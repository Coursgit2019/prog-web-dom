<?php

require_once('tp2-helpers.php');

$lat = (int)$_GET['lat'];
$lon= (int)$_GET['lon'];
$top = (int)$_GET['top'];

$rows   = array_map('str_getcsv', file('bornes_wifi.csv'));
$header = array('name','adr','lon','lat');
$csv    = array();
$contain   = array();
$topFive    = array();
foreach($rows as $row) {
    $csv[] = array_combine($header, $row);
}

$grenette = geopoint( $lon, $lat);
foreach($csv as $borne) {
   array_push($topFive,distance($grenette, $borne));
}


array_multisort($topFive, $csv);
echo '<table>';
for ($i = 0; $i<$top; $i++)
{
    $toCheck = $csv[$i];
    $obj= json_decode(smartcurl("https://api-adresse.data.gouv.fr/reverse/?lon=".$toCheck['lon']."&lat=".$toCheck['lat'], 0));

    echo '<tr>';
        echo '<td>'.$obj->features[0]->properties->label.'</td>';
    echo '</tr>';
  }
echo '</table>';


?>