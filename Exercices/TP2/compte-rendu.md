% PW-DOM  Compte rendu de TP

# Compte-rendu du TP2

# Partie 1 : Points d'accès wifi

1) Il y a 68 points d'accès wifi dans grenoble (d'après la commande wc -l)

2) cat bornes_wifi.csv | cut -d, -f2 | sort | uniq -c | wc -l donne 58 points d'accès différents

cat bornes_wifi.csv | cut -d, -f2 | sort | uniq -c | sort -g donne la bibliothèque études comme étant le point comportant le plus de bornes wifi (5 pour être plus précis)

5) Il y en a 7. Le plus proche est 8 place Grenette, 1er étage (neptune), à 20,1 m de la place grenette

# Partie 2 : Antennes GSM

1) Il y en a 101 d'après la commande wc -l. On trouve plus d'informations dans les fichiers recensants les antennes GSM que dans ceux recensant les bornes wifi, notamment l'opérateur en charge de l'antenne, le réseau qu'elle diffuse (3g, 4g...) ou encore la présence ou non d'une microcell. On peut aussi constater la présence d'un ID spécifique à l'antenne ainsi qu'un numéro de support et un numéro de carte radio.
Dans le cadre d'une démarche Open Data, cela permet d'avoir accès à un plus grand nombre d'informations, on peut par exemple facilement déterminer quel opérateur s'occupe du plus grand 
nombre d'antennes et de combien il en a la charge.

2) La commande cat DSPE_ANT_GSM_EPSG4326.csv | cut -d ';' -f4 | sort | uniq -c | sort -g
nous permet de voir qu'il y a 4 opérateurs : FREE avec 18 antennes, BYG et ORA avec 26 antennes chacun, enfin SFR avec 30 antennes. 

3) Avant de vérifier si le document est valide, il faut vérifier qu'il est bien formé avec la commande suivante :
 xmllint doc.kml
 D'après cette dernière, le document est bien formé, il nous faut ensuite vérifier sa validité.
 Pour cela, il faut tout d'abord utilisé un fichier dtd, que l'on doit créer à partir du fichier kml utilisé precédemment, pour cela j'ai utilisé le site https://xml.mherman.org/.
 
 Pour finir, nous devons vérifier la validité du fichier nouvellement crée avec xmllint --valid fichierdtd.dtd

4) Le fichier KML est bien plus redondant que les autres, il est aussi bien moins compact. Là ou le fichier CSV ne fait qu'un peu plus d'une centaine de ligne, le fichier kml en compte plusieurs miliers. Il semble aussi bien plus difficile de l'utiliser puisque, au lieu d'être composé d'une structure simple délimitée en champs par des délimiteurs, le fichier kml comporte un nombre incalculables de balises placées les unes après les autres, et prend plusieurs dizaines de lignes pour chaque antenne là ou le fichier csv n'en prend qu'une seule. Le fichier KML semble donc très difficile à exploiter et à lire, il ne semble d'ailleurs pas vraiment fait pour ça.

## Participants 

* Arthur Palma
* Victor Allègre
* Joan Besante




