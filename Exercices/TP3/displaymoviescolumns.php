<html><body>
<?php 
include "./tp3-helpers.php";
$movieID = '550';       //Fight Club ID
$link = "movie/".$movieID;      
$movieVO = json_decode(tmdbget($link),true);        // get json file original version of movie
$movieVA = json_decode(tmdbget($link, ['language'=>'en']),true);        // get json file english version of movie
$movieFR = json_decode(tmdbget($link, ['language'=>'fr']),true);        // get json file french version of movie

$VOimg = '<img src="https://image.tmdb.org/t/p/w200'.$movieVO['poster_path'].'">';
$VAimg = '<img src="https://image.tmdb.org/t/p/w200'.$movieVA['poster_path'].'">';
$FRimg = '<img src="https://image.tmdb.org/t/p/w200'.$movieFR['poster_path'].'">';      //recover poster of movie

$movieVOkeys = array_keys($movieVO);
$movieVAkeys = array_keys($movieVA);
$movieFRkeys = array_keys($movieFR);        // get every keys of the array to use them later without foreach

$movieVO = array_values($movieVO);
$movieVA = array_values($movieVA);
$movieFR = array_values($movieFR);      // get every values of the array to use them later without foreach

echo '<table>';
for ($i=0; $i < count($movieVO); $i++) {        // count(movieVO) this allows to know how many values to iterate
    echo '<tr>';
    echo '<td>'.$movieVOkeys[$i].' : '.$movieVO[$i].'</td>';
    echo '<td>'.$movieVAkeys[$i].' : '.$movieVA[$i].'</td>';
    echo '<td>'.$movieFRkeys[$i].' : '.$movieFR[$i].'</td>';        //display every key and associate value for the 3 versions of the movie
    echo '</tr>';
}
echo '<tr> <td>'.$VOimg.'</td> <td>'.$VAimg.'</td> <td>'.$FRimg.'</td> </tr>';      // display poster of the movie
echo '</table>';

$link = "movie/".$movieID."/videos";
$movievideo = json_decode(tmdbget($link),true);     // get the json file of the video page of the movie

echo ' <iframe width="420" height="315"
src="https://www.youtube.com/embed/'.$movievideo['results'][0]['key'].'">       
</iframe> ';        // display embedded youtube video with the key recovered from the json file

?>
</body></html>