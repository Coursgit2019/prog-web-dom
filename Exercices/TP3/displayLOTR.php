<html>
<body>
    <?php
        include "./tp3-helpers.php";

        $link = "search/movie";
        $moviesLOTR = json_decode(tmdbget($link, ['query'=>'Le+seigneur+des+anneaux']),true);       //get the json file which contains all LOTR movies
        $nbmovies = $moviesLOTR["total_results"];
        for($i=0;$i<$nbmovies;$i++){
            $movie[$i]=$moviesLOTR["results"][$i];      // fill an array of every movies found from the research
        }
    ?>
    <table>
        <tr>
            <th> ID </th>
            <th> Date de sortie </th>
            <th> Titre </th>
        </tr>
        <?php
            for($i=0;$i<$nbmovies;$i++){        // loop to display every movie with id, release date and title
                echo '<tr>
                <td>'.$movie[$i]["id"].'</td>
                <td>'.$movie[$i]["release_date"].'</td>
                <td>'.$movie[$i]["title"].'</td>
                </tr>';
            }
        ?>
    </table>
</body>
</html>