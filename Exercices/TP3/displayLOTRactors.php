<html>
<body>
<?php
 include "./tp3-helpers.php";

$link = "";
$lotr1 = json_decode(tmdbget('movie/120/credits'),true);
$lotr2 = json_decode(tmdbget('movie/121/credits'),true);
$lotr3 = json_decode(tmdbget('movie/122/credits'),true);		// get the json file of the LOTR trilogy

$nbacteurs1 = sizeof($lotr1["cast"]);
$nbacteurs2 = sizeof($lotr2["cast"]);
$nbacteurs3 = sizeof($lotr3["cast"]);		// create arrays to fill them of every actors of each movie

$listeacteurs = array();

for($i=0;$i<$nbacteurs1;$i++){
	$acteurs1[$i] = $lotr1["cast"][$i];		// fill an array to use it for the display
	array_push($listeacteurs, $lotr1["cast"][$i]['name']);		// fill a list of all actors in the trilogy
}
for($i=0;$i<$nbacteurs2;$i++){
	$acteurs2[$i] = $lotr2["cast"][$i];		// fill an array to use it for the display
	array_push($listeacteurs, $lotr2["cast"][$i]['name']);		// fill a list of all actors in the trilogy
}
for($i=0;$i<$nbacteurs3;$i++){
	$acteurs3[$i] = $lotr3["cast"][$i];		// fill an array to use it for the display
	array_push($listeacteurs, $lotr3["cast"][$i]['name']);		// fill a list of all actors in the trilogy
}

$listeacteurs = array_count_values($listeacteurs);		// that function allows to count the frequency for every key, so we know how many movie(s) each actor has played

echo '<br><br><table>
<CAPTION> LOTR 1 </CAPTION>
<tr>
<th> Nom de l acteur </th>
<th> Rôle </th>
<th> Nombre d\'apparitions dans la trilogie </th>
</tr>';
for($i=0;$i<$nbacteurs1;$i++){		// display every actor of the first movie with a clickable name that leads to his TMDB page and how many movie he has played
	if($acteurs1[$i]["character"] != NULL){
		echo '
		<tr>
		<td> <a href="https://www.themoviedb.org/person/'.$acteurs1[$i]["id"].'">'.$acteurs1[$i]["name"].'</a> </td>
		<td>'.$acteurs1[$i]["character"].'</td>
        <td>'.$listeacteurs[$acteurs1[$i]["name"]].'</td>
		</tr>';
	}
}
echo '</table><br><br><table>
<CAPTION> LOTR 2 </CAPTION>
<tr>
<th> Nom de l acteur </th>
<th> Rôle </th>
<th> Nombre d\'apparitions dans la trilogie </th>
</tr>';
for($i=0;$i<$nbacteurs2;$i++){		// display every actor of the second movie with a clickable name that leads to his TMDB page and how many movie he has played
	if($acteurs1[$i]["character"] != NULL){
		echo '
		<tr>
		<td> <a href="https://www.themoviedb.org/person/'.$acteurs1[$i]["id"].'">'.$acteurs1[$i]["name"].'</a> </td>
		<td>'.$acteurs2[$i]["character"].'</td>
        <td>'.$listeacteurs[$acteurs1[$i]["name"]].'</td>
		</tr>';
	}
}
echo '</table><br><br><table>
<CAPTION> LOTR 3 </CAPTION>
<tr>
<th> Nom de l acteur </th>
<th> Rôle </th>
<th> Nombre d\'apparitions dans la trilogie </th>
</tr>';
for($i=0;$i<$nbacteurs1;$i++){		// display every actor of the third movie with a clickable name that leads to his TMDB page and how many movie he has played
	if($acteurs3[$i]["character"] != NULL){
		echo '
		<tr>
		<td> <a href="https://www.themoviedb.org/person/'.$acteurs1[$i]["id"].'">'.$acteurs1[$i]["name"].'</a> </td>
		<td>'.$acteurs3[$i]["character"].'</td>
        <td>'.$listeacteurs[$acteurs1[$i]["name"]].'</td>
		</tr>';
	}
}
?>
</table>
</body>
</html>