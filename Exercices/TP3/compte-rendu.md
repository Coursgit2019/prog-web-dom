## Attention ! L'envoi de notre TP sur le repo GIT avait échoué et nous ne l'avions remarqué que trop tard, aussi nous avons rendu ce TP un peu en retard.

% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : **Utilisation de _The Movie DataBase_**

## Participants 

* Arthur Palma
* Joan Besante
* Victor Allègre

## Mise en jambes

1. Ce qui nous est renvoyé par le serveur de TMDB est au format JSON, l'extension instalée nous permet de l'analyser et de se rendre compte qu'il s'agit de la fiche du film _Fight Club_ comme on peut le voir sous le label *original_title*. </br> En rajoutant le paramètre `language=fr` on peut voir que les informations demandées telles que le résumé du film sont envoyées en français.

2. Lorsqu'on effectue la même requête que précédemment mais avec `curl` on s'aperçoit que le service nous renvoie la même chose que sur le navigateur internet c'est-à-dire en format JSON. En utilisant la fonction fournie dans le fichier helper on obtient également le même résultat.

## Les choses sérieuses

4. Voir displaymoviescolumns.php.
5. Voir displaymoviescolumns.php.
6. Voir displayLOTR.php.
7. Voir displayLOTRactors.php.
   
8. Nous n'avons pas pu trouver de solutions afin d'afficher les acteurs qui jouent les hobbits car dans les crédits du film il n'est nullement indiqué si tel acteur joue un hobbit ou non donc il nous paraît impossible de pouvoir différencier ces acteurs des autres.
9.  Voir displayLOTRactors.php.
10. Voir displaymoviescolumns.php. 

__NB__ : si vous avez des difficultés à générer les résultats il y a toujours les fichiers HTML qu'on a créé grâce aux résultats d'exécution des fichiers php et qui permettent de visualiser les résultats finaux.
