<?php

require_once('libcalcul.php');

if ($argc==4){
  $somme = (int)$argv[1];
  $taux = (int)$argv[2];
  $duree = (int)$argv[3];
  $cumul = (int)cumul($somme, $taux, $duree);
  echo "La somme est de ". (int)($cumul);
}
else{
  echo "Mauvais nombre d'arguments";
}
?>
